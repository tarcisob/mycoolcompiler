package Compiler;

import java_cup.runtime.*;
import java.io.IOException;

import Compiler.CPPSym;
import static Compiler.CPPSym.*;

%%

%public
%class CPPLex

%unicode
%line
%column

// %public
%final
// %abstract

%cupsym CPPSym
%cup
// %cupdebug

%init{
	// TODO: code that goes to constructor
%init}

%{
	 private Symbol symbol(int type) {
       return new Symbol(type, yyline, yycolumn);
    }
       
    private Symbol symbol(int type, Object val) {
      return new Symbol(type, yyline, yycolumn, val);
    }

	private void error()
	throws IOException{
		throw new IOException("illegal text at line = "+yyline+", column = "+yycolumn+", text = '"+yytext()+"'");
	}
%}

/*Prefixes and Suffixes*/
LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator}* | ([ \t\f])*

/*Digit and Numerical Types*/
UnsignedSuffix = [uU]
LongSuffix = [lL]
LongLongSuffix = ll | LL
IntegerSuffix = {UnsignedSuffix}{LongSuffix}? 
				| {UnsignedSuffix}{LongLongSuffix}?
				| {LongSuffix}{UnsignedSuffix}?
				| {LongLongSuffix}{UnsignedSuffix}?

NonZeroDigit = [1-9]
Digit = [0-9]
DecimalLiteral =  {NonZeroDigit}{Digit}*

OctalDigit = [0-7]
OctalLiteral = 0 {OctalDigit}*

HexadecimalDigit = {NonDigit} | {Digit}
HexadecimalLiteral = 0[xX]{HexadecimalDigit}+

/*float*/
FloatingSuffix = [FfLl]
Sign = [+-]
DigitSequence = {Digit}+
ExponentPart =  [eE]{Sign}?{DigitSequence}
FractionalConstant = {DigitSequence}? . {DigitSequence}  
					|{DigitSequence} .
					
FloatingLiteral = {FractionalConstant} {ExponentPart}? {FloatingSuffix}? 
				  | {DigitSequence} {ExponentPart} {FloatingSuffix}?
				  
/*Boolean*/
BooleanLiteral = false|true

//Pointer literal
PointerLiteral = nullptr

/* User-defined_Integer_Literal*/


UdSuffix = {Identifier}

NonDigit = [a-zA-Z] | _

IdentifierHelper = {Digit} | {IdentifierNonDigit}

Identifier =    {IdentifierHelper}* {IdentifierNonDigit} 
				| {IdentifierHelper}* {Digit}
				 
IdentifierNonDigit = {NonDigit} | {UniversalCharacterName}

UniversalCharacterName = \\u{hexQuad} |\\U{hexQuad}{hexQuad}

hexQuad = {HexadecimalDigit}{HexadecimalDigit}{HexadecimalDigit}{HexadecimalDigit}

doubleQuote = \"\" 							

SimpleEscapeSequence = \\\'
 						| \\\"
 						| \\\?
 						| \\\\
 						| \\a
 						| \\b
 						| \\f
 						| \\n
 						| \\r
 						| \\t
						| \\v
						
OctalEscapeSequence = \\ {OctalDigit}
						| \\ {OctalDigit} {OctalDigit}
						| \\ {OctalDigit} {OctalDigit} {OctalDigit}
						
HexadecimalEscapeSequence = \\x {HexadecimalDigit} {HexadecimalDigit}*

EscapeSequence = {SimpleEscapeSequence} | {OctalEscapeSequence} | {HexadecimalEscapeSequence}
				
SChar =  [^\"\\|\n]
		| {EscapeSequence}
		| {UniversalCharacterName}

SCharSequence = {SChar}+

EncodingPrefix = u8|u|U|L  

DCharSequence = {DChar}+

WhiteSpace = [\ \t\f]


DChar = [^\ \t\f\(\)\\|\n|\r\n]

RChar = [^\)([^\r\n|\r\n|\t\f\(\)\\])*\"]

RCharSequence = {RChar}+

CChar = [^\'\\|\n|\r\n]
 		|{EscapeSequence}
 		| {UniversalCharacterName}
 		
HChar = [^\n|\r\n|>]	

Q_CHAR = [^\n|\r\n|\"]


RawString = \"{DCharSequence}? \( {RCharSequence}? \) {DCharSequence}? \"

StringLiteral = {EncodingPrefix}? \" {SCharSequence}? \"
 				| {EncodingPrefix}? R {RawString} 

UserDefinedStringLiteral = {StringLiteral} {UdSuffix}



/*User_define_character_literal*/

CharacterLiteral = 	\' {CCharSequence} \'
 					| u \' {CCharSequence} \'
 					| U \' {CCharSequence} \'     
 					| L \' {CCharSequence} \'
 	
 CCharSequence = 	{CChar}+
 
EnumKey = enum
 			| enum class     
 			| enum struct
 			
UserDefineCharacterLiteral = {CharacterLiteral} {UdSuffix}

PpNumberHelper = 	({Digit} | {IdentifierNonDigit} | e {Sign} | E {Sign} | .)
			
					
PpNumber = {Digit} {PpNumberHelper}*| . {Digit} {PpNumberHelper}*;			
			


						

LParen = [^\ \t\f] \(

%x COMMENTS
%%
<YYINITIAL> {

 	"alignas"								{return symbol(CPPSym.ALIGN_AS, new String(yytext()));}
	"alignof"								{return symbol(CPPSym.ALIGNOF, new String(yytext()));}
	"and"									{return symbol(CPPSym.AND, new String(yytext()));}
 	"and_eq"								{return symbol(CPPSym.AND_EQ, new String(yytext()));}
	"asm"									{return symbol(CPPSym.ASM, new String(yytext()));}
 	"auto"									{return symbol(CPPSym.AUTO, new String(yytext()));}
 	"bitand"								{return symbol(CPPSym.BITAND, new String(yytext()));}
 	"bitor"									{return symbol(CPPSym.BITOR, new String(yytext()));}
 	"bool"									{return symbol(CPPSym.BOOL, new String(yytext()));}
	"break"									{return symbol(CPPSym.BREAK, new String(yytext()));}
	"case"									{return symbol(CPPSym.CASE, new String(yytext()));}
	"catch"									{return symbol(CPPSym.CATCH, new String(yytext()));}
 	"char"									{return symbol(CPPSym.CHAR, new String(yytext()));}
 	"char16_t"								{return symbol(CPPSym.CHAR16_T, new String(yytext()));}
 	"char32_t"								{return symbol(CPPSym.CHAR32_T, new String(yytext()));}
 	"class"									{return symbol(CPPSym.CLASS, new String(yytext()));}
 	"compl"									{return symbol(CPPSym.COMPL, new String(yytext()));}
 	"const" 								{return symbol(CPPSym.CONST, new String(yytext()));}
 	"const_cast" 							{return symbol(CPPSym.CONST_CAST, new String(yytext()));}
	"constexpr"								{return symbol(CPPSym.KEYWORD, new String(yytext()));}
	"continue"								{return symbol(CPPSym.CONTINUE, new String(yytext()));}
 	"decltype"								{return symbol(CPPSym.DECLTYPE, new String(yytext()));}
	"default"								{return symbol(CPPSym.DEFAULT, new String(yytext()));}
	"define"								{return symbol(CPPSym.DEFINE, new String(yytext()));}
	"delete"								{return symbol(CPPSym.DELETE, new String(yytext()));}
   	"dynamic_cast" 							{return symbol(CPPSym.DYNAMIC_CAST, new String(yytext()));}
	"do"									{return symbol(CPPSym.DO, new String(yytext()));}
 	"double"								{return symbol(CPPSym.DOUBLE, new String(yytext()));}
 	"elif"									{return symbol(CPPSym.ELIF, new String(yytext()));}
	"else"									{return symbol(CPPSym.ELSE, new String(yytext()));}
 	"endif"									{return symbol(CPPSym.ENDIF, new String(yytext()));}
   	"enum" 									{return symbol(CPPSym.ENUM, new String(yytext()));}
   	"error" 								{return symbol(CPPSym.ERROR, new String(yytext()));}
   	"explicit" 								{return symbol(CPPSym.EXPLICIT, new String(yytext()));}
	"export"								{return symbol(CPPSym.EXPORT, new String(yytext()));}
	"extern"								{return symbol(CPPSym.EXTERN, new String(yytext()));}
	"false"									{return symbol(CPPSym.FALSE, new String(yytext()));}
   	"final"									{return symbol(CPPSym.FINAL, new String(yytext()));}
 	"float"									{return symbol(CPPSym.FLOAT, new String(yytext()));}
	"for"									{return symbol(CPPSym.FOR, new String(yytext()));}
	"friend"								{return symbol(CPPSym.FRIEND, new String(yytext()));}
	"goto"									{return symbol(CPPSym.GOTO, new String(yytext()));}
	"if"									{return symbol(CPPSym.IF, new String(yytext()));}
	"ifdef"									{return symbol(CPPSym.IFDEF, new String(yytext()));}
	"ifndef"								{return symbol(CPPSym.IFNDEF, new String(yytext()));}
	"inline"								{return symbol(CPPSym.INLINE, new String(yytext()));}
	"include"								{return symbol(CPPSym.INCLUDE, new String(yytext()));}
 	"int"									{return symbol(CPPSym.INT, new String(yytext()));}
 	"line"									{return symbol(CPPSym.LINE, new String(yytext()));}
 	"long"									{return symbol(CPPSym.LONG, new String(yytext()));}
	"mutable"								{return symbol(CPPSym.MUTABLE, new String(yytext()));}
	"namespace"								{return symbol(CPPSym.NAMESPACE, new String(yytext()));}
	"new"									{return symbol(CPPSym.NEW, new String(yytext()));}
	"noexcept"								{return symbol(CPPSym.NOEXCEPT, new String(yytext()));}
 	"not"									{return symbol(CPPSym.NOT, new String(yytext()));}
 	"not_eq"								{return symbol(CPPSym.NOT_EQ, new String(yytext()));}
	"nullptr"								{return symbol(CPPSym.NULLPTR, new String(yytext()));}
 	"operator"								{return symbol(CPPSym.OPERATOR, new String(yytext()));}
 	"or"									{return symbol(CPPSym.OR, new String(yytext()));}
 	"or_eq"									{return symbol(CPPSym.OR_EQ, new String(yytext()));}
 	"pragma"								{return symbol(CPPSym.PRAGMA, new String(yytext()));}
 	"private"								{return symbol(CPPSym.PRIVATE, new String(yytext()));}
 	"protected"								{return symbol(CPPSym.PROTECTED, new String(yytext()));}
 	"public"								{return symbol(CPPSym.PUBLIC, new String(yytext()));}
	"register"								{return symbol(CPPSym.REGISTER, new String(yytext()));}
 	"reinterpret_cast" 						{return symbol(CPPSym.REINTERPRET_CAST, new String(yytext()));}
	"return"								{return symbol(CPPSym.RETURN, new String(yytext()));}
 	"short"									{return symbol(CPPSym.SHORT, new String(yytext()));}
 	"signed"								{return symbol(CPPSym.SIGNED, new String(yytext()));}
	"sizeof"								{return symbol(CPPSym.SIZEOF, new String(yytext()));}
	"static"								{return symbol(CPPSym.STATIC, new String(yytext()));}
	"static_assert"							{return symbol(CPPSym.STATIC_ASSERT, new String(yytext()));}
 	"static_cast" 							{return symbol(CPPSym.STATIC_CAST, new String(yytext()));}
 	"struct"								{return symbol(CPPSym.STRUCT, new String(yytext()));}
	"switch"								{return symbol(CPPSym.SWITCH, new String(yytext()));}
   	"template"								{return symbol(CPPSym.TEMPLATE, new String(yytext()));}
   	"this"									{return symbol(CPPSym.THIS, new String(yytext()));}
	"thread_local"							{return symbol(CPPSym.THREAD_LOCAL, new String(yytext()));}
	"throw"									{return symbol(CPPSym.THROW, new String(yytext()));}
	"true"									{return symbol(CPPSym.TRUE, new String(yytext()));}
	"try"									{return symbol(CPPSym.TRY, new String(yytext()));}
	"typedef"								{return symbol(CPPSym.TYPEDEF, new String(yytext()));}
 	"typeid" 								{return symbol(CPPSym.TYPE_ID, new String(yytext()));}
 	"typename" 								{return symbol(CPPSym.TYPENAME, new String(yytext()));}
 	"undef"									{return symbol(CPPSym.UNDEF, new String(yytext()));}
 	"union"									{return symbol(CPPSym.UNION, new String(yytext()));}
 	"unsigned"								{return symbol(CPPSym.UNSIGNED, new String(yytext()));}
	"using"									{return symbol(CPPSym.USING, new String(yytext()));}
 	"virtual"								{return symbol(CPPSym.VIRTUAL, new String(yytext()));}
 	"void"									{return symbol(CPPSym.VOID, new String(yytext()));}
 	"volatile"								{return symbol(CPPSym.VOLATILE, new String(yytext()));}
 	"wchar_t"								{return symbol(CPPSym.WCHAR_T, new String(yytext()));}
	"while"									{return symbol(CPPSym.WHILE, new String(yytext()));}
	"xor"									{return symbol(CPPSym.XOR, new String(yytext()));}
 	"xor_eq"								{return symbol(CPPSym.XOR_EQ, new String(yytext()));}
	"new [ ]"								{return symbol(CPPSym.NEW_WITH_BRACKETS, new String(yytext()));}
 	"delete [ ]"							{return symbol(CPPSym.DELETE_WITH_BRACKETS, new String(yytext()));}
		 
	/*Prefixes and Suffixes*/
		
	"("										{return symbol(CPPSym.OPENING_PARENTHESIS, new String(yytext()));}
   	")"										{return symbol(CPPSym.CLOSING_PARENTHESIS, new String(yytext()));}
    ","										{return symbol(CPPSym.COMMA, new String(yytext()));}
    "?"										{return symbol(CPPSym.QUESTION_MARK, new String(yytext()));}
    ":"										{return symbol(CPPSym.COLON, new String(yytext()));}
    "|"										{return symbol(CPPSym.PIPE, new String(yytext()));}
    "&"										{return symbol(CPPSym.AMPERSAND, new String(yytext()));}
    "^"										{return symbol(CPPSym.CIRCUNFLEX, new String(yytext()));}
    "="										{return symbol(CPPSym.EQUAL, new String(yytext()));}
    "!"										{return symbol(CPPSym.EXCLAMATION_POINT, new String(yytext()));}
    "<"										{return symbol(CPPSym.LESS_THAN, new String(yytext()));}
    ">"										{return symbol(CPPSym.GREATER_THAN, new String(yytext()));}
    "+"										{return symbol(CPPSym.PLUS, new String(yytext()));}
    "-"										{return symbol(CPPSym.MINUS, new String(yytext()));}
    "*"										{return symbol(CPPSym.STAR, new String(yytext()));}
    "/"										{return symbol(CPPSym.SLASH, new String(yytext()));}
    "%"										{return symbol(CPPSym.PERCENT, new String(yytext()));}
    "."										{return symbol(CPPSym.DOT, new String(yytext()));}
    "[" 									{return symbol(CPPSym.OPENING_SQ_BRACKET, new String(yytext()));}
    "]" 									{return symbol(CPPSym.CLOSING_SQ_BRACKET, new String(yytext()));}
    "{" 									{return symbol(CPPSym.OPENING_CURLY_BRACKET, new String(yytext()));}
    "}" 									{return symbol(CPPSym.CLOSING_CURLY_BRACKET, new String(yytext()));}
    "::"									{return symbol(CPPSym.DOUBLE_COLON, new String(yytext()));}
    "..."									{return symbol(CPPSym.TRIPLE_DOT, new String(yytext()));}
    ">="									{return symbol(CPPSym.GREATER_THAN_EQUAL, new String(yytext()));}
    "<="									{return symbol(CPPSym.LESS_THAN_EQUAL, new String(yytext()));}
    "=="									{return symbol(CPPSym.DOUBLE_EQUAL, new String(yytext()));}
    "!="									{return symbol(CPPSym.EXCLAMATION_POINT_EQUAL, new String(yytext()));}
    "++"									{return symbol(CPPSym.DOUBLE_PLUS, new String(yytext()));}
    "--"									{return symbol(CPPSym.DOUBLE_MINUS, new String(yytext()));}
    "~"										{return symbol(CPPSym.TILDE, new String(yytext()));}
    "::~"								    {return symbol(CPPSym.DOUBLE_COLON_TILDE, new String(yytext()));}
    "&&"									{return symbol(CPPSym.DOUBLE_AMPERSAND, new String(yytext()));}
    "+="									{return symbol(CPPSym.PLUS_EQUAL, new String(yytext()));}
    "-="									{return symbol(CPPSym.MINUS_EQUAL, new String(yytext()));}
 	"*="									{return symbol(CPPSym.STAR_EQUAL, new String(yytext()));}
	"/="									{return symbol(CPPSym.SLASH_EQUAL, new String(yytext()));}
	"%="									{return symbol(CPPSym.PERCENTE_EQUAL, new String(yytext()));}
	"^="									{return symbol(CPPSym.CIRCUNFLEX_EQUAL, new String(yytext()));}
	"&="									{return symbol(CPPSym.AMPERSAND_EQUAL, new String(yytext()));}
	"|="									{return symbol(CPPSym.PIPE_EQUAL, new String(yytext()));}
	"<<"									{return symbol(CPPSym.DOUBLE_LESS_THAN, new String(yytext()));}
	">>"									{return symbol(CPPSym.DOUBLE_GREATER_THAN, new String(yytext()));}
	">>="									{return symbol(CPPSym.DOUBLE_GREATER_THAN_EQUAL, new String(yytext()));}
	"<<="									{return symbol(CPPSym.DOUBLE_LESS_THAN_EQUAL, new String(yytext()));}
	"||"									{return symbol(CPPSym.DOUBLE_PIPE, new String(yytext()));}
	"->*"									{return symbol(CPPSym.MINUS_GREATER_THAN_STAR, new String(yytext()));}
	"->"									{return symbol(CPPSym.MINUS_GREATER_THAN, new String(yytext()));}
	/*"()"									{return symbol(CPPSym.OPENING_CLOSING_PARENTHESIS, new String(yytext()));}*/
	"[]"									{return symbol(CPPSym.OPENING_CLOSING_SQ_BRACKETS, new String(yytext()));}
 	{doubleQuote}									{return symbol(CPPSym.DOUBLE_QUOTE, new String(yytext()));}
 	"#"										{return symbol(CPPSym.POUND, new String(yytext()));}
	"##"									{return symbol(CPPSym.DOUBLE_POUND, new String(yytext()));}
  	"<:"									{return symbol(CPPSym.LESS_THAN_COLON, new String(yytext()));}
 	":>"									{return symbol(CPPSym.COLON_GREATER_THAN, new String(yytext()));}
 	"<%"									{return symbol(CPPSym.LESS_THAN_PERCENT, new String(yytext()));}
 	"%>"									{return symbol(CPPSym.PERCENT_GREATER_THAN, new String(yytext()));}
 	"%:"									{return symbol(CPPSym.PERCENT_COLON, new String(yytext()));}
 	"%:%:"									{return symbol(CPPSym.PERCENT_COLON_PERCENT_COLON, new String(yytext()));}
 	";"										{return symbol(CPPSym.SEMI_COLON, new String(yytext()));}
 	".*"									{return symbol(CPPSym.DOT_STAR, new String(yytext()));}
 
   	/*Digit and Numerical Types*/
   
    {DecimalLiteral}						{return symbol(CPPSym.DECIMAL_LITERAL, new String(yytext()));}
	{HexadecimalLiteral}					{return symbol(CPPSym.HEXADECIMAL_LITERAL, new String(yytext()));}
    {OctalLiteral}							{return symbol(CPPSym.OCTAL_LITERAL, new String(yytext()));}
    {IntegerSuffix}							{return symbol(CPPSym.INTEGER_SUFFIX, new String(yytext()));}
    {FloatingLiteral}						{return symbol(CPPSym.FLOATING_LITERAL, new String(yytext()));}
    
    
    {CharacterLiteral}						{return symbol(CPPSym.CHARACTER_LITERAL, new String(yytext()));}
    {StringLiteral}							{return symbol(CPPSym.STRING_LITERAL, new String(yytext()));}
    {Identifier}							{return symbol(CPPSym.IDENTIFIER, new String(yytext()));}
  	
   
   	{LineTerminator}						{ /* ignore */ }
  	{WhiteSpace}                            { /* ignore */ } 
   
   /*
     {LineTerminator}						{ /* ignore */ }
	{NewLine}								{return symbol(CPPSym.NEW_LINE, new String(yytext()));}
    {PpNumber}								{return symbol(CPPSym.PP_NUMBER, new String(yytext()));}
   
     {LParen}								{return symbol(CPPSym.LPAREN, new String(yytext()));}
   */ 
  
/* <COMMENTS> {
    {HChar}								{return symbol(CPPSym.H_CHAR, new String(yytext()));}
	{Q_CHAR}							{return symbol(CPPSym.Q_CHAR, new String(yytext()));}
	}   
*/
}

<<EOF>>                                       { return symbol(CPPSym.EOF); }