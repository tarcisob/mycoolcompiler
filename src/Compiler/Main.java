package Compiler;

import java.io.File;
import java.io.FileReader;

import semantic.Logger;


public class Main {

	public static void main(String argv[]) {

		CPPLex l = null;
		//String argumento = argv[i];
		String argumento = "teste.txt";
				
		//for (int i = 0; i < argv.length; i++) {
		for (int i = 0; i < 1; i++){
			File file = new File(argumento);

			if (!file.exists()) {
				String filepath = argumento;
				
				System.out.println(filepath);
				file = new File(filepath);

				if (!file.exists()) {
					System.out.println("File " + argumento
							+ " not found.");
					continue;
				}
			}

			try {
			System.out.println("Parsing [" + argumento + "]");
//			System.out.println("Logging level: " + argv[0]);
			Logger.getInstance().setLoggingLevel(argv[0].equals("0")? Logger.OFF : argv[0].equals("1")? Logger.ERROR : argv[0].equals("2")? Logger.INFO : Logger.DEBUG);
//			System.out.println("Logging level just set to: " + Logger.getInstance().getLoggingLevel());
			l = new CPPLex(new FileReader(file));
			CPPParser p = new CPPParser(l);
			p.parse();
			
			
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}